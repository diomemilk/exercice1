FROM python:3.8-alpine3.14
WORKDIR /app 
ADD requirements.txt . 
RUN pip install -r requirements.txt 
ADD index.py .
CMD [ "index:app"]